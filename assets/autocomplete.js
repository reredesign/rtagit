function RTagItSource(request, response) {
    var input = $(this.element);
    var url = input.attr('data-source');
    var exclude = input.val();
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: 'q=' + request.term + '&exclude=' + exclude,
        success: function (data) {
            response(data);
        }
    });
}