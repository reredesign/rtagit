<?php
YiiBase::import('zii.widgets.jui.CJuiInputWidget');

class RTagIt extends CJuiInputWidget
{
	public $tagItCssFile = true;
	public $options = array();
	public $scriptPos = CClientScript::POS_READY;

	public $autoCompleteUrl;

	public function init()
	{
		parent::init();
		$resolved = $this->resolveNameID();
		$this->id = $resolved[1];
		$this->publishAssets();
	}

	public function run()
	{
		if(is_array($this->model->{$this->attribute}))
			$value = implode(',', $this->model->{$this->attribute});
		CHtml::resolveNameID($this->model, $this->attribute, $this->htmlOptions);

		if(isset($this->autoCompleteUrl)) {
			$this->options['tagSource'] = 'js:RTagItSource';
			$this->htmlOptions['data-source'] = CHtml::normalizeUrl($this->autoCompleteUrl);
		}

		echo CHtml::textField($this->htmlOptions['name'], $value, $this->htmlOptions);

		$jsOptions = CJavaScript::encode($this->options);

		/** @var CClientScript $clientScript */
		$clientScript = Yii::app()->clientScript;

		$clientScript->registerScript($this->id . '_init', <<<JAVASCRIPT
		$('#{$this->id}').tagit({$jsOptions});
JAVASCRIPT
			, $this->scriptPos);
	}


	public function publishAssets()
	{
		/** @var CAssetManager $assetManager */
		$assetManager = Yii::app()->assetManager;
		/** @var CClientScript $clientScript */
		$clientScript = Yii::app()->clientScript;

		$jsDir = dirname(__FILE__) . '/vendor/tag-it/js';
		$jsPath = $assetManager->publish($jsDir, false, -1, true);
		$clientScript->registerScriptFile($jsPath . "/tag-it.js");
		if(isset($this->autoCompleteUrl)) {
			$assetsDir = dirname(__FILE__) . '/assets';
			$assetsPath = $assetManager->publish($assetsDir, false, -1 ,true);
			$clientScript->registerScriptFile($assetsPath . "/autocomplete.js");
		}

		$cssDir = dirname(__FILE__) . '/vendor/tag-it/css';
		$cssPath = $assetManager->publish($cssDir);
		$clientScript->registerCssFile($cssPath . '/jquery.tagit.css');
		if ($this->tagItCssFile) {
			$clientScript->registerCssFile($cssPath . '/tagit.ui-zendesk.css');
		}

	}

}